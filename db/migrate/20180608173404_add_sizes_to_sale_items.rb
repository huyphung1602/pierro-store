class AddSizesToSaleItems < ActiveRecord::Migration
  def change
    add_column :sale_items, :sizes, :string
  end
end
