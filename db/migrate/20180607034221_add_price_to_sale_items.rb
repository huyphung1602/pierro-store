class AddPriceToSaleItems < ActiveRecord::Migration
  def change
    add_column :sale_items, :price, :integer
  end
end
