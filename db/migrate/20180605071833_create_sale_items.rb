class CreateSaleItems < ActiveRecord::Migration
  def change
    create_table :sale_items do |t|
      t.string :name
      t.string :description
      t.string :image_url
      t.string :quanity
      t.string :status

      t.timestamps null: false
    end
  end
end
