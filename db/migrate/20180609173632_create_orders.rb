class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :sale_items_code
      t.string :customer_name
      t.string :phone
      t.string :address
      t.string :note
      t.integer :total_payment

      t.timestamps null: false
    end
  end
end
