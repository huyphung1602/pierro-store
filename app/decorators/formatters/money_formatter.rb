module Formatters
  class MoneyFormatter
    def self.vnd_formatter(price)
      price.to_s.gsub(/(\d)(?=(\d{3})+$)/,'\1,') + 'VND'
    end
  end
end