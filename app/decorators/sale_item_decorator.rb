class SaleItemDecorator < Draper::Decorator
  delegate_all
  decorates :sale_item

  def vnd_price
    Formatters::MoneyFormatter.vnd_formatter(price)
  end

  def sizes_array
    sizes.split(',')
  end

  def simple_first_sentence
    description.split('.').first + '.'
  end
end
