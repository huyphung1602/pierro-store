module OrderHelper
  def vnd_price(money)
    Formatters::MoneyFormatter.vnd_formatter(money)
  end
end
