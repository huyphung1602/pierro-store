require 'json'

module ApplicationHelper
  def cart_is_not_empty?
    JSON.parse(cookies[:sale_item_ids]).present? if !cookies[:sale_item_ids].nil?
  end

  def semantic_class_for flash_type
    { success: 'positive message', error: 'negative message', notice: 'warning message', info: 'info message'}[flash_type.to_sym]
  end

  def flash_messages(opts = {})
    flash.map do |msg_type, message|
      content_tag(:div, message, class: "ui #{semantic_class_for(msg_type)}") do
        content_tag(:i, ''.html_safe, class: 'close icon', data: {dismiss: 'message'}) + content_tag(:div, message)
      end
    end.join.html_safe
  end
end
