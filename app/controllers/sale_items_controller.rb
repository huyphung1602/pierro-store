class SaleItemsController < ApplicationController
  def show
    @sale_item = sale_item.decorate
  end

  private

  def sale_item
    @sale_item = SaleItem.find(params[:id])
  end
end
