class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :signed_in?, :require_signin

  def current_user
    return @current_user if @current_user
    @current_user ||= User.find(session[:user_id])
  end

  def require_signin
    if !signed_in?
      flash[:warning] = I18n.t 'session.warning'
      redirect_to new_session_path
    end
  end

  def signed_in?
    current_user
  end
end
