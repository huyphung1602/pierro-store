class OrdersController < ApplicationController
  def new
    if cookies[:sale_item_ids].nil?
      order_items_hash = {}
    else
      order_items_hash = JSON.parse(cookies[:sale_item_ids])
    end
    item_ids = order_items_hash.map { |order_item| order_item.first.to_i }
    order_item_groups = order_item_groups(item_ids)
    @order_items = generate_order_items(order_items_hash, order_item_groups)[0]
    @total = generate_order_items(order_items_hash, order_item_groups)[1]
  end

  def create
    @order = Order.new new_order_attributes
    if @order.save
      cookies.permanent[:sale_item_ids] = JSON.generate({})
      flash[:success] = I18n.t 'order.successfully'
      redirect_to :root
    else
      flash[:error] = I18n.t 'order.fail'
      redirect_to :new_order
    end
  end

  private

  def order_item_groups(item_ids)
    @order_item_groups ||= SaleItem.where(id: item_ids)
                             .select(:id, :name, :price, :image_url)
                             .group_by(&:id)
  end

  def generate_order_items(order_items_hash, order_item_groups)
    order_items = []
    total = 0

    order_items_hash.each do |key, value|
      size = key.split('_')[1]
      quanity = value
      item = order_item_groups[key.to_i].first
      price = item.price.to_i * quanity.to_i
      total += price

      order_items << {
        id: item.id,
        key: key,
        name: "#{item.name} Size #{size}",
        image_url: item.image_url,
        quanity: quanity,
        price: price
      }
    end

    [order_items, total]
  end

  def new_order_attributes
    {
      sale_items_code: generate_sale_items_code,
      customer_name: params[:name].strip,
      phone: params[:phone].strip,
      address: params[:address].strip,
      note: params[:note].strip,
      total_payment: params[:total]
    }
  end

  def generate_sale_items_code
    params[:order_items].map do |order_item|
      "#{order_item[:key]}_#{order_item[:quanity]}"
    end.join(',')
  end
end
