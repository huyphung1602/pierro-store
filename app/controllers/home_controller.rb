class HomeController < ApplicationController
  def index
    @sale_items = SaleItem.all.map(&:decorate)
  end
end
