class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: params[:email])
    if @user
      if @user.authenticate(params[:password])
        flash.now[:success] = I18n.t 'session.signed_in'
        session[:user_id] = @user.id
        redirect_to root_path
      else
        flash.now[:error] = I18n.t 'session.wrong_password'
        render 'new'
      end
    else
      flash.now[:error] = I18n.t 'session.not_found'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_path, flash: {info: 'Signed out'}
  end
end