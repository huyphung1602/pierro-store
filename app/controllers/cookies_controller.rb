require 'json'

class CookiesController < ApplicationController
  def add_item_to_cart
    redirect_to root_path unless params[:sale_item_id].present?
    cookies[:sale_item_ids] = JSON.generate({}) if cookies[:sale_item_ids].nil?
    cookies.permanent[:sale_item_ids] = JSON.generate(calculate_quanity(params[:quanity]))
    if params[:commit] == 'add'
      redirect_to action: 'show', controller: 'sale_items', id: params[:sale_item_id]
    else
      redirect_to :new_order
    end
  end

  def delete_item_from_cart
    sale_items_hash = JSON.parse(cookies[:sale_item_ids])
    sale_items_hash.delete(params[:delete_key])
    cookies.permanent[:sale_item_ids] = JSON.generate(sale_items_hash)
    redirect_to :new_order
  end

  private

  def encoded_key
    "#{params[:sale_item_id]}_#{params[:size]}"
  end

  def calculate_quanity(quanity)
    sale_items_hash = JSON.parse(cookies[:sale_item_ids])
    sale_items_hash[encoded_key] = 0 if sale_items_hash[encoded_key].nil?
    sale_items_hash[encoded_key] += quanity.to_i if sale_items_hash[encoded_key] < 10
    sale_items_hash
  end
end
