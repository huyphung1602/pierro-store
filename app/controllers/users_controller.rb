class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new new_user_attributes
    if @user.save
      flash[:success] = I18n.t 'user.successfully'
      session[:user_id] = @user.id
      redirect_to :root
    else
      render :new
    end
  end

  private

  def new_user_attributes
    params.require(:user).permit(:name, :email, :password)
  end
end