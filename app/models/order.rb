class Order < ActiveRecord::Base
  validates :customer_name, presence: true
  validates :phone, presence: true
  validates :address, presence: true
end
