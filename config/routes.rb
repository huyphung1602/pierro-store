Rails.application.routes.draw do

  root 'home#index'

  resources :sale_items
  resources :orders

  resources :sessions, only: [:new, :create]
  delete 'sign_out' => 'sessions#destroy'
  resources :users

  post 'add_item_to_cart' => 'cookies#add_item_to_cart'
  post 'delete_item_from_cart' => 'cookies#delete_item_from_cart'
end
